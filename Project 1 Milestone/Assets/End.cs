﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End : MonoBehaviour
{
    private PlayerMovement character;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            character.enabled = !character.enabled;
        }
    }
}