﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public GameObject myBackground;
     

    void Awake()
    {
        myBackground.SetActive(true);
    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Q))
        {
            myBackground.SetActive(false);
        }
    }
}
